#! /usr/bin/env python2

import argparse
import os
import subprocess

def download( application, version):
   print( "downloading: application: [" + application + "], version: [" + version + "]")
   # Do the downloading
   filename = application + '-' + version + '.zip'
   return filename

def unpack( filename, destination=None):

   if not os.path.exists( filename):
      raise SystemError( filename + ": No such file")

   if not destination:
      import tempfile
      destination = tempfile.mkdtemp()

   if not os.path.exists( destination):
      raise SystemError( destination + ": No such directory")

   import zipfile
   zip_ref = zipfile.ZipFile(filename, 'r')
   zip_ref.extractall(destination)
   zip_ref.close()
   
   return destination


def execute_manuscript( destination):

   manuscript = destination + '/manuscript'

   if not os.path.exists( manuscript):
      raise SystemError( "No manuscript found in " + destination)

   print( "running manuscript: " + manuscript)
   os.chmod(manuscript, 0o755)
   output = subprocess.check_output( [manuscript])

   print output,

def main():
   parser = argparse.ArgumentParser()

   parser.add_argument("-v", "--verbose", help="Running in verbose mode", action="store_true")
   parser.add_argument("application", help="Which application to install")
   parser.add_argument("version", help="Which version to install")

   args = parser.parse_args()

   if args.verbose:
      print "vebosing"

   application = args.application
   version = args.version

   filename = download( application, version)
   destination = unpack( filename)

   execute_manuscript( destination)

if __name__ == "__main__":
    main()