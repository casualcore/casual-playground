## Overview
This is a little take on a new build system using a model to represent dependencies and targets/tasks/operation.

## Files
* **make** - the main 'command'
* build/
    - **core**   - core functionality in dsl
    - **model**  - contains model and handling of it
    - **recipe** - core recipe functionality
    - **types** - common types
* user/
    - **api**    - represent a user extention