from contextlib import contextmanager
import os
import subprocess
import errno
import re
import sys
import casual.make.tools.color as color_module
import cStringIO


def importCode(code, name, add_to_sys_modules=0):
    """ code can be any object containing code -- string, file object, or
       compiled code object. Returns a new module object initialized
       by dynamically importing the given code and optionally adds it
       to sys.modules under the given name.
    """
    import imp
    module = imp.new_module(name)

    if add_to_sys_modules:
        import sys
        sys.modules[name] = module
    exec code in module.__dict__

    return module

def reformat( line):
    """ reformat output from make and add som colours"""
    
    for regex in reformat.ingore_filters:
        match = regex.match( line)
        if match:
            return ''
     
    for regex, filter in reformat.filters:
        match = regex.match( line)
      
        if match:
            return filter( match)
        
    return line


reformat.ingore_filters = [
   re.compile(r'(^make.*)Nothing to be done for'),
]

reformat.filters = [ 
    [ re.compile(r'(^(g|c|clang)\+\+).* -o (\S+\.o) (\S+\.cc|\S+\.cpp|\S+\.c).*'),
     lambda match: color_module.color.green( 'compile: ') + color_module.color.white( match.group(4)) + '\n' ],
    [ re.compile(r'(^(g|c|clang)\+\+).* -E .*?(\S+\.cc|\S+\.cpp|\S+\.c).*'),
     lambda match: color_module.color.green( 'dependency: ') + color_module.color.white( match.group(3)) + '\n' ],
    [ re.compile(r'(^ar) \S+ (\S+\.a).*'),  
     lambda match: color_module.color.blue( 'archive: ') + color_module.color.white( match.group(2)) + '\n' ],
    [ re.compile(r'(^(g|c|clang)\+\+).* -o (\S+) (?:(\S+\.o) ).*'),
     lambda match: color_module.color.blue( 'link: ') + color_module.color.white( match.group(3)) + '\n' ],
    [ re.compile(r'^(.*[.]cmk )(.*)'),  
     lambda match: color_module.color.cyan( 'makefile: ') + color_module.color.blue( match.group(2) ) + ' ' + match.group(1) + '\n'],
    [ re.compile(r'(^make.*:)(.*)'),  
     lambda match: color_module.color.header( match.group(1) ) + match.group(2) + '\n'],
    [ re.compile(r'^rm -f (.*)'),  
     lambda match: color_module.color.header( 'delete: ' ) + match.group(1) + '\n' ],
    [ re.compile(r'^mkdir( [-].+)*[ ](.*)'),  
     lambda match: color_module.color.header( 'create: ' ) + match.group( 2) + '\n' ],
    [ re.compile(r'.*casual-build-server[\S\s]+-c (\S+)[\s]+-o (\S+) .*'),  
     lambda match: color_module.color.blue( 'buildserver: ' ) + color_module.color.white( match.group(2)) + '\n' ],
    [ re.compile(r'^copy +(\S+) (.*)'),  
     lambda match: color_module.color.blue( 'prepare install: ') + color_module.color.white( match.group(1)) +  ' --> ' +  color_module.color.white(match.group(2)) + '\n' ],
    [ re.compile(r'^(>[a-zA-Z+.]+)[\s]+(.*)'),  
     lambda match: color_module.color.green( 'updated: ') + match.group(2) + ' ' + color_module.color.blue( match.group(1)) + '\n' ],
    [ re.compile(r'^generates makefile.*from[ ](.*)'),  
     lambda match: color_module.color.cyan( 'makefile: ') + color_module.color.yellow( 'generate ') + color_module.color.white( match.group(1)) + '\n' ],
    [ re.compile(r'^casual-build-resource-proxy.*--output[ ]+([^ ]+)'),  
     lambda match: color_module.color.blue( 'build-rm-proxy: ') + color_module.color.white( match.group(1)) +'\n' ],
    [ re.compile(r'^ln -s (.*?) (.*)'),  
     lambda match: color_module.color.blue( 'symlink: ') + color_module.color.white( match.group(2)) + ' --> ' + color_module.color.white( match.group(1)) + '\n' ],
    [ re.compile(r'^[^ ]*clang-tidy (.*?) (.*)'),  
     lambda match: color_module.color.green( 'lint: ') + color_module.color.white( match.group(1)) + '\n' ],
    
]

@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)

def create_directory( directory):
   """ 
   We need this construction to avoid race conditions using multiple processes
   That is instead of checking first.
   """
   try:
      os.makedirs(directory)
   except OSError as e:
      if e.errno != errno.EEXIST:
         raise

def execute( command, show_command = True, show_output = True):

   output_result = cStringIO.StringIO()

   if show_command:
      if "CASUAL_RAW_FORMAT" in os.environ:
         sys.stdout.write( " ".join( command) + '\n')
      else:
         sys.stdout.write( reformat( " ".join( command)))
   
   output = subprocess.PIPE

   if "CASUAL_MAKE_DRY_RUN" not in os.environ:
      line = None
      process = subprocess.Popen( command, bufsize=0, stdout=output, stderr=subprocess.STDOUT, universal_newlines=True)

      while process.poll() is None:

         if process.stdout:
            line = reformat( process.stdout.readline())

         if show_output:
            output_result.write( line)

      #
      # Manually "flush" stdout...
      # 
      if process.stdout:
         line = reformat( process.stdout.readline())

      while( line):
         if process.stdout:
            line = reformat( process.stdout.readline())
            output_result.write( line)

      reply = output_result.getvalue()
      output_result.close()

      if process.poll() > 0:
         print( reply)
         raise SystemError("\naborted due to errors\n")

      return reply
   return ""


def execute_command( cmd, name = None, directory = None, show_command = True, show_output = True):
   try:
      if directory:
         with cd(directory):
            if name:
               create_directory( os.path.dirname( name.filename))
            return execute( cmd, show_command, show_output)
      else:
         return execute( cmd, show_command, show_output)
   except subprocess.CalledProcessError as e:
      raise SystemError( e.output)