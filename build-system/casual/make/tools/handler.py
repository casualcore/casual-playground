from multiprocessing import Queue, Process

import multiprocessing as mp
import signal
import casual.make.recipe as recipe
import sys
import os

def need_serial_execution( action_list):
   """
   Find out if any item in list require serial handling
   """
   if os.getenv("CASUAL_SERIAL_EXECUTION"):
      return True
      
   for item in action_list:
      if item.need_serial_execution:
         return True
   return False

def handle( action_list):
   """
   Handle the action list in parallel or in serial
   """

   if need_serial_execution( action_list):
      serial( action_list)
   else:
      parallel( action_list)

def worker( input, output):
   for item in iter(input.get, 'STOP'):
      try:
         recipe.dispatch( item)
         output.put(0)
      except:
         output.put(1)
         sys.exit(1)

def terminate_children( process):
   for p in process:
      if p.is_alive():
         p.join()

def empty( task_queue, reply_queue):
   while not task_queue.empty():
      task_queue.get()
   while not reply_queue.empty():
      reply_queue.get()

def parallel( action_list):
   """
   Handle action list in parallel
   """

   process = []

   try:
      # Create queues
      task_queue = Queue()
      reply_queue = Queue()

      # Submit tasks
      for task in action_list:
         task_queue.put(task)

      # Start worker processes
      process = []
      for dummy in range( mp.cpu_count()):
         p = Process(target=worker, args=(task_queue, reply_queue))
         p.daemon = True
         p.start()
         process.append(p)

      for dummy in range(len(action_list)):
         if reply_queue.get() == 1:
            empty( task_queue, reply_queue)
            terminate_children( process)
            raise SystemError("compiler error")

      terminate_children( process)

   except KeyboardInterrupt:
      print("\nCaught KeyboardInterrupt, terminating workers")
      empty( task_queue, reply_queue)
      terminate_children( process)
      raise SystemExit( -1)

def serial( action_list):
   """
   Handle action list in serial
   """
   
   for item in action_list:
      recipe.dispatch( item)
