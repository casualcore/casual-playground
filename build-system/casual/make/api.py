import inspect
import os

import casual.make.model as model
import casual.make.recipe as recipe
from casual.make.target import Target, Recipe
from casual.make.tools.executor import importCode

#
# global setup for operations
#
compile_target = model.register( 'compile')
link_target = model.register( 'link')
link_library_target = model.register( 'link-library')
link_archive_target = model.register( 'link-archive')
link_executable_target = model.register( 'link-executable')
link_unittest_target = model.register( 'link-unittest')

test_target = model.register('test')
# this operations should be done no matter what when referenced
test_target.execute = True
test_target.need_serial = True

clean_target = model.register( 'clean')
# this operations should be done no matter what when referenced
clean_target.execute = True
clean_target.need_serial = True

dependency_target = model.register( 'dependency')
dependency_target.execute = True
dependency_target.need_serial = False

link_target.add_dependency( [link_library_target, link_archive_target, link_executable_target, link_unittest_target])

install_target = model.register('install')
# this operations should be done no matter what when referenced
install_target.execute = True
install_target.need_serial = True

import importlib
compiler_handler = os.getenv("CASUAL_COMPILER_HANDLER")
compiler_handler_module = importlib.import_module( compiler_handler)

#
# Helpers
#
def caller():

   name = inspect.getouterframes( inspect.currentframe())[2][1]
   return os.path.abspath( name)

def normalize_library_target( libs, paths = None):
   reply = []
   for lib in libs:
      if not isinstance( lib, Target):
         target = model.get( lib, paths)
         if not target:
            lib = model.register( lib)
         else:
            lib = target
      reply.append( lib)
   return reply

# Helpers
def includes( dependency_file, makefile):

   context_directory, dummy = os.path.split(makefile)
   if os.path.exists( dependency_file):
      with open( dependency_file ) as file:
         files = file.read()
      files = files.replace('\\\n','\n')
      reply=[]
      for f in (files.split()[1:]):
         filename = f.rstrip()
         if os.path.isabs( filename):
            abs_path = filename
         else:
            abs_path = os.path.abspath( context_directory + '/' + filename)
         item = model.store.register( abs_path, abs_path, makefile)

         reply.append( item)
      return reply
   else:
      return []

#
# Main DSL
#
def Compile( source, destination = None):
   """
   Compile code to object files
   """
   makefile = caller()

   if not destination:
      destination = compiler_handler_module.make_objectname( source)

   dependencyfile = compiler_handler_module.make_dependencyfilename( destination)
   dependencyfile_target = model.register(name=dependencyfile, filename=dependencyfile, makefile = makefile) 
   dependencies = includes( dependencyfile, makefile = makefile)

   source_target = model.register(name=source, filename=source, makefile = makefile)
   
   if len( dependencies) == 0:
      # no dependency file - add at least source file
      dependencies = [source_target]

   object_target = model.register(name=destination, filename=destination, makefile = makefile)
   
   arguments = { 
      'destination' : object_target, 
      'dependencyfile' :  dependencyfile,
      'source' : source_target, 
      'include_paths' : model.include_paths( makefile)
      }

   object_target.add_recipe( Recipe( recipe.create_includes, arguments))
   object_target.add_recipe( Recipe( recipe.compile, arguments))

   object_target.add_dependency( dependencies)

   compile_target.add_dependency( object_target)

   clean_target.add_recipe( Recipe( recipe.clean, {'filename' : [ object_target, dependencyfile_target], 'makefile': makefile}))
 
   dependencyfile_target.add_recipe( Recipe( recipe.create_includes, arguments))
   dependencyfile_target.execute = True
   dependency_target.add_dependency( dependencyfile_target)

   return object_target

def Link( destination, objects, libs):
   """
   Link object files to shared objects
   """

   return LinkLibrary( destination, objects, libs)

def LinkLibrary( destination, objects, libs):
   """
   Link object files to shared objects library
   """
   
   makefile = caller()
   directory, dummy = os.path.split( makefile)
   name = os.path.basename(destination)
   full_library_name = compiler_handler_module.expanded_library_name( destination, directory)
   library_target = model.register(name=name, filename=full_library_name, makefile = makefile)

   library_paths = model.library_paths( makefile)
   normalized_library_targets = normalize_library_target( libs, paths = library_paths)
   arguments = {
      'destination' : library_target, 
      'objects' : objects, 
      'libraries': normalized_library_targets, 
      'library_paths': library_paths
      }

   library_target.add_recipe( Recipe( recipe.link_library, arguments))
   library_target.add_dependency( objects + normalized_library_targets)

   link_library_target.add_dependency( library_target)

   clean_target.add_recipe( Recipe( recipe.clean, {'filename' : [library_target], 'makefile': makefile}))

   return library_target

def LinkArchive( destination, objects):
   """
   Link object files to archive
   """
   
   makefile = caller()
   directory, dummy = os.path.split( makefile)
   name=os.path.basename(destination)

   full_archive_name = compiler_handler_module.expanded_archive_name( destination, directory)
   archive_target = model.register(name=name, filename=full_archive_name, makefile = makefile)
   arguments = {
      'destination' : archive_target, 
      'objects' : objects
      }

   archive_target.add_recipe ( Recipe( recipe.link_archive, arguments))
   archive_target.add_dependency( objects)

   link_archive_target.add_dependency( archive_target)

   clean_target.add_recipe( Recipe( recipe.clean, {'filename' : [archive_target], 'makefile': makefile}))

   return archive_target

def LinkExecutable( destination, objects, libs):
   
   makefile = caller()

   full_executable_name = compiler_handler_module.expanded_executable_name(destination)
   executable_target = model.register( full_executable_name, full_executable_name, makefile = makefile)
   library_paths = model.library_paths( makefile)
   normalized_library_targets = normalize_library_target( libs, library_paths)
   arguments = {
      'destination' : executable_target, 
      'objects' : objects, 
      'libraries': normalized_library_targets, 
      'library_paths': library_paths
      }

   executable_target.add_recipe( Recipe( recipe.link_executable, arguments))
   executable_target.add_dependency( objects + normalized_library_targets)

   link_executable_target.add_dependency( executable_target)

   clean_target.add_recipe( Recipe( recipe.clean, {'filename' : [executable_target], 'makefile': makefile}))

   return executable_target

def LinkUnittest( destination, objects, libs):
   
   makefile = caller()

   full_executable_name = compiler_handler_module.expanded_executable_name(destination)
   executable_target = model.register( full_executable_name, full_executable_name, makefile = makefile)

   library_paths = model.library_paths( makefile)
   normalized_library_targets = normalize_library_target( libs, library_paths) + normalize_library_target([ 'gtest', 'gtest_main' ])
   arguments = {
      'destination' : executable_target, 
      'objects' : objects, 
      'libraries': normalized_library_targets, 
      'library_paths': library_paths
      }

   executable_target.add_recipe( Recipe( recipe.link_unittest, arguments))
   executable_target.add_dependency( objects + normalized_library_targets)

   link_unittest_target.add_dependency( executable_target)

   clean_target.add_recipe( Recipe( recipe.clean, {'filename' : [executable_target], 'makefile': makefile}))
 
   test_executable_target = model.register( "test-" + destination, destination, makefile = makefile)
   test_executable_target.add_recipe( Recipe( recipe.test,
      {
         'destination' : executable_target,
         'library_paths': library_paths
      }) 
   )
   test_executable_target.execute = True

   test_executable_target.add_dependency( executable_target)
   test_target.add_dependency( test_executable_target)

   return executable_target

def Install( source, path):
   
   makefile = caller()
   if not source:
      return None

   if not isinstance( source, list):
      source = [source]

   for item in source:
      if isinstance( item, tuple):
         install_file_target = model.register( 'install_' + item[0], makefile = makefile)
         dependency_target = model.get( item[0])
         if not dependency_target:
            filename = os.path.abspath( os.path.dirname( makefile) + '/' + item[0])
            dependency_target = model.register( item[0], filename, makefile = makefile)
         install_file_target.add_dependency( dependency_target)
         install_file_target.execute = True
         arguments = {
            'source' : dependency_target.filename, 
            'path' : path + '/' + item[1]
         }
         install_file_target.add_recipe( Recipe( recipe.install, arguments))
         install_target.add_dependency( install_file_target)
      elif isinstance( item, model.Target):
         install_file_target = model.register( 'install_' + item.name, makefile = makefile)
         dependency_target = item
         install_file_target.add_dependency( dependency_target)
         install_file_target.execute = True
         arguments = {
            'source' : dependency_target.filename, 
            'path' : path
         }
         install_file_target.add_recipe( Recipe( recipe.install, arguments))
         install_target.add_dependency( install_file_target)
      else:
         install_file_target = model.register('install_' + item, makefile = makefile)
         dependency_target = model.get( item)
         if not dependency_target:
            filename = os.path.abspath( os.path.dirname( makefile) + '/' + item)
            dependency_target = model.register( item, filename, makefile = makefile)
         install_file_target.add_dependency( dependency_target)
         install_file_target.execute = True
         arguments = {
            'source' : dependency_target.filename, 
            'path' : path 
         }
         install_file_target.add_recipe( Recipe( recipe.install, arguments))
         install_target.add_dependency( install_file_target)


def Environment( variabel, value):
   """
   Set a environment variable

   TODO: Implement
   """
   pass

def IncludePaths( paths):
   """
   Add 'extra' include paths 
   """
   makefile = caller()
   model.add_key_value( makefile, 'include_paths', paths)

def LibraryPaths( paths):
   """
   Add 'extra' library paths 
   """
   makefile = caller()
   model.add_key_value( makefile, 'library_paths', paths)

def Dependencies(target, dependencies):
   """
   Adding a possibility to setup a more 'soft' dependency
   """
   makefile = caller()

   if not isinstance( target, Target):
      target = model.register(name=target, filename=target, makefile=makefile)

   if not isinstance( dependencies, list):
      raise SystemError("dependencies is " + str(dependencies) + " must be a list")

   dependency_list = []
   for dependency in dependencies:
      dependency_list.append( dependency)

   target.add_dependency( dependency_list)

def Build( filename):
   """
   Build a separate file
   """

   makefile = caller()

   directory, dummy = os.path.split( makefile)
   if directory:
      filename = directory + '/' + filename

   file = open( filename)
   importCode(file, "makefile", 1)


   


