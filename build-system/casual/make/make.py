
import sys
import subprocess
import os
import time
import platform

import pprint

import argparse

def usage():
   print ( "usage: "+ sys.argv[0] + " target" )
   raise SystemExit



class Time(object):
   def __init__(self):
      self.start = time.time()
      self.delta = self.start

   def lap(self):
      stop = time.time()
      print "lap: ", (stop - self.delta)
      self.delta = stop

   def stop(self):
      self.lap()
      print "tot: ",(self.delta - self.start)

def size_of_action_list( levels, action_list):
   """
   calculate size of nested type action_list
   """
   size = 0
   for level in levels:
      size = size + len( action_list[ level])
 
   return size

def main():

   parser = argparse.ArgumentParser()
   parser.add_argument( "target", help="The target to handle")
   parser.add_argument( "-d","--dry-run", help="Just show the tasks to be done", action="store_true")
   parser.add_argument( "-r","--raw_format", help="Echo command in raw format", action="store_true")
   parser.add_argument( "-c","--compiler", help="choose compiler", default="g++")
   parser.add_argument( "--compiler_handler", help="choose compiler module directly", default=None)
   parser.add_argument( "-p", "--parallel", help="compile in a parallel maner", action="store_true", default=False)
   args = parser.parse_args()
   if len(sys.argv) < 2:
      usage()

   selected=args.target

   if args.dry_run:
      os.environ["CASUAL_MAKE_DRY_RUN"] = "1"

   if args.raw_format:
      os.environ["CASUAL_RAW_FORMAT"] = "1"

   if args.compiler_handler: 
      os.environ["CASUAL_COMPILER_HANDLER"] = args.compiler_handler

   elif args.compiler == 'g++':
      if platform.system() == 'Darwin':
         module = "casual.make.platforms.osx"
      elif platform.system() == 'Linux':
         module = "casual.make.platforms.linux"
      os.environ["CASUAL_COMPILER_HANDLER"] = module
   elif args.compiler == 'cl':
      os.environ["CASUAL_COMPILER_HANDLER"] = "casual.make.platforms.windows"
   else:
      raise SystemError("Compilerhandler not given or not supported")

   if not args.parallel:
      os.environ["CASUAL_SERIAL_EXECUTION"] = "1"

   try:

      # Need to import this after argparse
      import casual.make.model as model
      import casual.make.tools.handler as handler

      #
      # Build the actual model from a file
      #
      print "building model..."
      #time = Time()
      model.build()
      #time.lap()
      selected_target = model.get( selected)
      
      if not selected_target:
         raise SystemError( selected + " not known")

      model.construct_dependency_tree( selected_target)
      #time.lap()
      action_list = model.construct_action_list( selected_target)
      #time.stop()

      #
      # Start traversing thru action_list in reverse order. 
      #
      levels = sorted( action_list.keys(), reverse = True)
      size = size_of_action_list( levels, action_list)
      
      if size > 0:
         print "Starting handling tasks..."
      else:
         print "Nothing to do"

      for level in levels:
         #
         # All actions within a 'level' can be done in parallel
         #
         handler.handle( action_list[ level])
            
   except SystemError as e:
      print e
      raise SystemExit(1)   

if __name__ == "__main__":
   main()