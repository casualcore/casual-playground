import inspect
import pprint
import os
import time

from casual.make.target import Target
from casual.make.tools.executor import importCode

# globals
class Store( object):
   def __init__(self):
      # actual model
      self.m_model = { 'target' : {}, 'key_value' : {}}
      # target cache
      self.m_cache = {}

   def model(self):
      return self.m_model

   def get(self, name, filename = None, paths = None):
      """
      Fetch target from model
      """
      # First - exact match
      if name in self.m_cache and filename in self.m_cache[name]:
         return self.m_cache[name][filename]
      # Second - match by correct path
      elif paths:
         for path in paths:
            for fname in self.m_cache[name]:
               if path in fname:
                  return self.m_cache[name][fname]
      # Third - correct name - no other options - pick first
      elif name in self.m_cache and not filename:
         for f in self.m_cache[name]:
            return self.m_cache[name][f]
      return None

   def register(self, name, filename = None, makefile = None):
      """
      Create and register target in model
      """
      if isinstance( name, Target):
         target = self.get( name.name, name.filename)
         if target:
            return target
         self.m_cache[name.name][name.filename] = name
         return name
      else:
         target = self.get( name, filename)
         if target:
            return target
         else:
            if name not in self.m_cache:
               self.m_cache[name] = {}
            self.m_cache[name][filename] = Target( name, filename, makefile)
            return self.m_cache[name][filename]

   def dump(self):
      pprint.pprint( self.m_cache)

# instance of the global store
store = Store()

def register( name, filename = None, makefile = None):
   return store.register( name, filename, makefile)

def get( name, filename = None, paths = None):
   return store.get( name)

def dump():
   pprint.pprint( store.model())

def add_key_value( makefile, key, value):
   """
   Add key, value in model
   """
   if makefile not in store.model()['key_value']:
      store.model()['key_value'][makefile] = {}
   store.model()['key_value'][makefile][key] = value

def get_value( makefile, key):
   """
   Get value with key from model 
   """
   if makefile in store.model()['key_value'] and key in store.model()['key_value'][makefile]:
      return store.model()['key_value'][makefile][key]
   else:
      return None

def make_absolute_path( paths, directory):
   """
   Normalize path in path list
   """
   reply = []
   for path in paths:
      if os.path.isabs(path):
         reply.append( path)
      else:
         reply.append( os.path.abspath( directory + '/' + path))
   return reply

def include_paths( makefile):
   value = get_value( makefile, 'include_paths')
   if value:
      return value
   else:
      return []

def library_paths( makefile):
   value = get_value( makefile, 'library_paths')
   directory, dummy = os.path.split( makefile)
   if value:
      return make_absolute_path( value, directory)
   else:
      return []

def construct_dependency_tree( target):
   """
   Construcs a depedency tree in the view of the target_rhs
   """
   if not isinstance( target, Target):
      target = store.get( target)
   
   return analyze_dependency_tree( target)


def calculate_max_timestamp( target):
   """
   Retrive max timestamp
   """
   timestamp = 0.0

   for item in target.dependency:
      if item.timestamp > timestamp:
         timestamp = item.timestamp
   return timestamp

analyze_cache = {}
def analyze_dependency_tree( target):
   """
   Analyzing tree for actions to take
   Returns: is action required? True/False
   """

   if target in analyze_cache:
      return analyze_cache[target]

   if not target.dependency or len(target.dependency) == 0:
      analyze_cache[target] = target.execute
      return target.execute
   else:
      action_needed = False
      current_target = target

      for child_target in current_target.dependency:
         action_required = analyze_dependency_tree( child_target)

         if current_target:

            if action_required:
               #
               # The depedency steps is already evalutated to be run
               # So we need to run this one too.
               #
               current_target.execute = True
               action_needed = True
               continue

            timestamp = current_target.timestamp
            if current_target.filename and not timestamp:
               #
               # No timestamp, means no file.
               # Need to run this step
               #
               current_target.execute = True
               action_needed = True
               continue
               
            else:
               max_timestamp = calculate_max_timestamp( current_target)
               if current_target.filename and timestamp < max_timestamp:
                  #
                  # The dependency files is newer.
                  # Need to run this step
                  #
                  current_target.execute = True
                  action_needed = True
                  continue

   analyze_cache[target] = action_needed
   return action_needed

def construct_action_list( target):
   """
   Construct a action list based on the dependency tree
   """

   data = {}
   def create_action_list( target, level = 0):
      """
      Create the action list
      """
      level = level + 1

      for item in target.dependency:
         create_action_list( item, level)
         
      if target.execute and target.has_recipes():
         if not level in data:
            data[level] = []
         data[level].append( target)
      return data

   def remove_duplicates( unfiltered):
      """
      Creating a new list without duplicates
      """
      cached = {}
      actions = {}
      for item in sorted( unfiltered.keys(), reverse = True):
         subaction = []
         for subitem in unfiltered[item]:
            if subitem not in cached:
               subaction.append( subitem)
               cached[subitem] = True
         if subaction:
            if item not in actions:
               actions[item] = []
            actions[item] = subaction

      return actions

   if target.execute and target.has_recipes():
      if not 0 in data:
         data[0] = []
      data[0].append( target)

   action_list = create_action_list( target)
   purged_list = remove_duplicates( action_list)

   return purged_list

def build():
   """
   Build the model from a file
   """
   #
   # Open the default name 'makefile.cmk'
   #
   # Only supported option right now
   #
   file = open("makefile.cmk")
   importCode(file, "makefile", 1)
