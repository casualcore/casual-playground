import pprint

import distutils.file_util

import os
import time
import sys

from casual.make.target import Target
import casual.make.tools.executor as executor



import importlib
compiler_handler = os.getenv("CASUAL_COMPILER_HANDLER")
selector = importlib.import_module( compiler_handler)

action_list={}
callback_list=[]

def find(file, paths):
   for p in paths:
      if os.path.exists(p + '/' + file):
         return p + '/' + file
   return None

def make_files( objects):
   files = []
   for item in objects:
      if isinstance( item, Target):
         files.append( item.filename)
      else:
         files.append( item)
   return files

def dump():
   pprint.pprint( action_list)


#
# registred recipes
#

def create_includes( input):
   """
   Recipe for creating dependency includes
   """
   source =  input['source']
   destination = input['destination']
   context_directory = os.path.dirname( input['destination'].makefile)
   include_paths = input['include_paths']
   dependency_file = input['dependencyfile']

   selector.create_includes( source, destination, context_directory, include_paths, dependency_file)

def compile( input):
   """
   Recipe for compiling
   """
   source =  input['source']
   destination = input['destination']
   include_paths = input['include_paths']
   context_directory = os.path.dirname( input['destination'].makefile)

   selector.create_compile( source, destination, context_directory, include_paths)
  

def link( input):
   """
   Recipe for linking objects to libraries
   """

   destination = input['destination']
   objects = make_files( input['objects'])
   context_directory = os.path.dirname( input['destination'].makefile)

   # choose correct flavor of linking
   libraries = input['libraries']
   library_paths = input['library_paths']

   selector.create_link_library( destination, context_directory, objects, library_paths, libraries)
  
def link_library( input):
   """
   Recipe for linking objects to libraries
   """

   destination = input['destination']
   objects = make_files( input['objects'])
   context_directory = os.path.dirname( input['destination'].makefile)

   # choose correct flavor of linking
   libraries = input['libraries']
   library_paths = input['library_paths']

   selector.create_link_library( destination, context_directory, objects, library_paths, libraries)

def link_executable( input):
   """
   Recipe for linking objects to executables
   """

   destination = input['destination']
   objects = make_files( input['objects'])
   context_directory = os.path.dirname( input['destination'].makefile)

   libraries = input['libraries']
   library_paths = input['library_paths']

   selector.create_link_executable( destination, context_directory, objects, library_paths, libraries)


def link_archive( input):
   """
   Recipe for linking objects to archives
   """

   destination = input['destination']
   objects = make_files( input['objects'])
   context_directory = os.path.dirname( input['destination'].makefile)

   selector.create_link_archive( destination, context_directory, objects)

def link_unittest( input):
   """
   Recipe for linking objects to executables
   """
   destination = input['destination']
   objects = make_files( input['objects'])
   context_directory = os.path.dirname( input['destination'].makefile)

   libraries = input['libraries']
   library_paths = input['library_paths']

   selector.create_link_executable( destination, context_directory, objects, library_paths, libraries)


def test( input):
   testfile = input['destination']
   library_paths = input['library_paths']
   cmd = ["env", "LD_LIBRARY_PATH=$LD_LIBRARY_PATH:" + ":".join(library_paths)] + [testfile, "--gtest_color=yes"]

   executor.execute_command( cmd)


def install( input):
   source = input['source']
   if isinstance( source, basestring):
      pass
   elif isinstance( source, Target):
      source = source.filename
   else:
      source = " ".join( source)

   path = input['path']
   if "MAKE_DRY_RUN" not in os.environ:
      (filename, copied) = distutils.file_util.copy_file( source, path, update=1, verbose=0)
      if copied:
         sys.stdout.write( executor.reformat('copy ' + source + ' ' + filename))
   else:
      sys.stdout.write( executor.reformat('copy ' + source + ' ' + path))


def clean( input):
   file = input['filename']
   context_directory = os.path.dirname( input['makefile'])

   with executor.cd(context_directory):
      if isinstance( file, Target):
         filename = file.filename
         if os.path.exists( filename):
            sys.stdout.write( executor.reformat( "rm -f " + filename))
            if "MAKE_DRY_RUN" not in os.environ:
               os.remove( filename)
      elif isinstance( file, basestring):
         if os.path.exists( file):
            sys.stdout.write( executor.reformat( "rm -f " + file))
            if "MAKE_DRY_RUN" not in os.environ:
               os.remove( file)
      else:
         for f in file:
            if isinstance( f, basestring):
               filename = f
            else:
               filename = f.filename
            if os.path.exists( filename):
               sys.stdout.write( executor.reformat( "rm -f " + filename))
               if "MAKE_DRY_RUN" not in os.environ:
                  os.remove( filename)

def dispatch( target):
   """
   This is the core dispatch function
   """      
   if target.recipes:
      [ recipe.function( recipe.arguments) for recipe in target.recipes]


